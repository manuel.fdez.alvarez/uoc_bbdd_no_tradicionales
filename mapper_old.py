#!/usr/bin/env python
import sys

for line in sys.stdin:
    line       = line.strip()
    key_value  = line.split(",")
    key_in     = key_value[0]
    value_in   = key_value[1]

    #if (value_in == 'TNT' or value_in.isdigit()):
    print( '%s\t%s' % (key_in, value_in) )

    #cat datasets/* | python3 mapper.py | sort | python3 reducer.py | sort | python3 reducer2.py