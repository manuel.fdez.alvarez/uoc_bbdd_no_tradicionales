var Programas = [
    {
        programa: "Almost_News",
        canal:"ABC"
    },
    {
        programa: "Hourly_Show",
        canal:"TLF"
    },
    {
        programa: "Hot_Cooking",
        canal:"LSX"
    },
    {
        programa: "Postmodern_Family",
        canal:"LSX"
    },
    {
        programa: "Baked_News",
        canal:"LSX"
    },
    {
        programa: "Dumb_Games",
        canal:"ABC"
    }
]

var Audiencias = [
    {
        programa: "Almost_News",
        audiencia:25
    },
    {
        programa: "Hourly_Show",
        audiencia:30
    },
    {
        programa: "Hot_Cooking",
        audiencia:7
    },
    {
        programa: "Almost_News",
        audiencia:35
    },
    {
        programa: "Postmodern_Family",
        audiencia:8
    },
    {
        programa: "Baked_News",
        audiencia:15
    },
    {
        programa: "Dumb_Games",
        audiencia:60
    }
]


Programas.map(c => {
    /**
     * Aplicamos un filtro para que los items sean los que corresponden a ese programa.
     * Corresponde al join entre las dos tablas: 
     *      from Programas P , Audiencias A
     *      where P.Programa = A.Programa
     */
    let totalValue = Audiencias.filter( 
        audiencia => audiencia.programa === c.programa
    ).reduce((sum,audiencia) => audiencia.audiencia + sum,0)

    return {
        name:c.canal,
        itemCount: totalValue}
})