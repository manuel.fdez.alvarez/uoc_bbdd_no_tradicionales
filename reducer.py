#!/usr/bin/env python
import sys

#Canal anterior
prev_TV_channel = ""
curr_TV_channel_cnt = 0

for line in sys.stdin:
    line       = line.strip() 
    key_value  = line.split(',')

    curr_TV_channel  = key_value[0]
    
    #Sumamos las audiencias
    audiences = 0
    for value in key_value[1:]:
        audiences  += int(value)

    if prev_TV_channel == "":
        prev_TV_channel =  curr_TV_channel

    #Si el canal leido, es diferente al que teniamos,
    #devolvemos el valor y reiniciamos el contador.
    if prev_TV_channel !=  curr_TV_channel:
        print ("{} {}".format(prev_TV_channel, curr_TV_channel_cnt))
        prev_TV_channel =   curr_TV_channel
        curr_TV_channel_cnt = int(audiences)
    #Si no, seguimos acumulando.
    else:
        curr_TV_channel_cnt+= int(audiences)

#Imprimimos el ultimo canal.
print ("{} {}".format(prev_TV_channel, curr_TV_channel_cnt))