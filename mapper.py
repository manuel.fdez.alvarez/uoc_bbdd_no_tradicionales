#!/usr/bin/env python
import sys

total = []

for line in sys.stdin:
    line       = line.strip() 
    key_value  = line.split(',')
      
    #Trabajamos solo con los canales y las aduencias.
    value_in   = key_value[1]
    #Si el valor es numerico, lo metemos en una lista
    if value_in.isdigit():
        total.append (value_in)
    #Si no, se trata de un canal, imprimimos el canal y el listado de elementos
    else:
        print ("{},{}".format(value_in,','.join(total))) # ABC,25,35
        total = [] #Reiniciamos el vector que contiene las audiencias.